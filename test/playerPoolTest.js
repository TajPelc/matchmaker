"use strict";

const assert = require('assert');
const pool = require('../src/matchmaking/pool');
const errors = require('../src/config/errors');
const playerMock = require('./mocks/player');

describe('playerPool', () => {
    let playerPool;

    beforeEach(() => {
        playerPool = pool.initPlayerPool('dota2');
    });

    describe('#addPlayer', () => {
        it('should add a player to the pool', () => {
            assert.equal(playerPool.getPoolSize(), 0);
            playerPool.addPlayer(playerMock.buildDotaPlayer(1));
            assert.equal(playerPool.getPoolSize(), 1);
        });
    });

    describe('#removePlayer', () => {
        it('should remove player from the pool', () => {
            const pewdiepie = playerMock.buildDotaPlayer(1);

            playerPool.addPlayer(pewdiepie);
            assert.equal(playerPool.getPoolSize(), 1);
            playerPool.removePlayer(pewdiepie);
            assert.equal(playerPool.getPoolSize(), 0);
        });

        it('should not remove a player that is not in the pool', () => {
            playerPool.addPlayer(playerMock.buildDotaPlayer(1));
            assert.equal(playerPool.getPoolSize(), 1);
            playerPool.removePlayer(playerMock.buildDotaPlayer(9, 8000));
            assert.equal(playerPool.getPoolSize(), 1);
        });
    });

    describe('#findNearestPlayer', () => {
        it("should find the closest player in skill to the given rating", () => {
            playerPool.addPlayer(playerMock.buildDotaPlayer(1, 1050));
            playerPool.addPlayer(playerMock.buildDotaPlayer(2, 920));
            playerPool.addPlayer(playerMock.buildDotaPlayer(3, 4322));
            playerPool.addPlayer(playerMock.buildDotaPlayer(4, 6999));
            playerPool.addPlayer(playerMock.buildDotaPlayer(5, 3222));
            playerPool.addPlayer(playerMock.buildDotaPlayer(6, 4356));

            const first = playerPool.findNearestPlayerFor(playerMock.buildDotaPlayer(7, 950));

            assert.equal(first.player.id, 2);
            assert.equal(first.distance, 30);

            const second = playerPool.findNearestPlayerFor(playerMock.buildDotaPlayer(8, 3000));

            assert.equal(second.player.id, 5);
            assert.equal(second.distance, 222);

            const third = playerPool.findNearestPlayerFor(playerMock.buildDotaPlayer(9, 6999));

            assert.equal(third.player.id, 4);
            assert.equal(third.distance, 0);
        });

        it("should not find an appropriate player due to distance", () => {
            playerPool.addPlayer(playerMock.buildDotaPlayer(1, 6999));
            playerPool.addPlayer(playerMock.buildDotaPlayer(2, 3222));

            assert.throws(() => { playerPool.findNearestPlayerFor(playerMock.buildDotaPlayer(3, 4000), 100); }, errors.NoSuitablePlayersFound);
        });

        it("it should not match itself", () => {
            const lonelyPlayer = playerMock.buildDotaPlayer(1, 6999);
            playerPool.addPlayer(lonelyPlayer);

            assert.throws(() => { playerPool.findNearestPlayerFor(lonelyPlayer); }, errors.EmptyPoolError);
        })
    })
});