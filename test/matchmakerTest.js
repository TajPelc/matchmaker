"use strict";

const Promise = require('bluebird');
const assert = require('assert');
const playerPool = require('../src/matchmaking/pool');
const constants = require('../src/config/constants');
const matchmaker = require('../src/matchmaking/engine');
const playerMock = require('./mocks/player');

describe('matchmaker', () => {
    describe('#findMatchForPlayer', () => {
        it('should ignore rating and match together four players that join at different times', (done) => {
            const matchmakerInstance = matchmaker.initMatchmaker(playerPool.initPlayerPool(constants.GAMES.DOTA2), {
                game: constants.GAMES.DOTA2,
                iterationDelay: 100, // 0.1s between iterations
                iterationDistance: null,
                maxTime: 2000 // 2s
            });

            let matchForPlayer1;
            let matchForPlayer2;
            let matchForPlayer3;
            let matchForPlayer4;

            matchForPlayer1 = matchmakerInstance.findMatchForPlayer(playerMock.buildDotaPlayer(1, 4382));

            matchForPlayer2 = Promise.delay(500).then(() => {
                return matchmakerInstance.findMatchForPlayer(playerMock.buildDotaPlayer(2, 2200));
            });

            matchForPlayer3 = Promise.delay(700).then(() => {
                return matchmakerInstance.findMatchForPlayer(playerMock.buildDotaPlayer(3, 3200));
            });

            matchForPlayer4 = Promise.delay(1000).then(() => {
                return matchmakerInstance.findMatchForPlayer(playerMock.buildDotaPlayer(4, 5500));
            });

            Promise.all([matchForPlayer1, matchForPlayer2, matchForPlayer3, matchForPlayer4]).then((results) => {
                assert.equal(results[0].player.id, results[1].opponent.player.id, "First player should be the second's opponent.");
                assert.equal(results[1].player.id, results[0].opponent.player.id, "Second player should be the first's opponent.");
                assert.equal(results[2].player.id, results[3].opponent.player.id, "Third player should be the fourth's opponent.");
                assert.equal(results[3].player.id, results[2].opponent.player.id, "Fourth player should be the third's opponent.");
                done();
            }).catch((err) => {
                done(err);
            });
        });

        it('should match players together that are closest by rating', (done) => {
            const matchmakerInstance = matchmaker.initMatchmaker(playerPool.initPlayerPool(constants.GAMES.DOTA2), {
                game: constants.GAMES.DOTA2,
                iterationDelay: 50,
                iterationDistance: 50,
                maxTime: 2000
            });

            let matchForPlayer1;
            let matchForPlayer2;
            let matchForPlayer3;
            let matchForPlayer4;

            matchForPlayer1 = matchmakerInstance.findMatchForPlayer(playerMock.buildDotaPlayer(1, 1100));

            matchForPlayer2 = Promise.delay(500).then(() => {
                return matchmakerInstance.findMatchForPlayer(playerMock.buildDotaPlayer(2, 4400));
            });

            matchForPlayer3 = Promise.delay(700).then(() => {
                return matchmakerInstance.findMatchForPlayer(playerMock.buildDotaPlayer(3, 5400));
            });

            matchForPlayer4 = Promise.delay(1000).then(() => {
                return matchmakerInstance.findMatchForPlayer(playerMock.buildDotaPlayer(4, 1500));
            });

            Promise.all([matchForPlayer1, matchForPlayer2, matchForPlayer3, matchForPlayer4]).then((results) => {
                assert.equal(results[0].player.id, results[3].opponent.player.id, "First player should be the fourths's opponent.");
                assert.equal(results[1].player.id, results[2].opponent.player.id, "Second player should be the thirds's opponent.");
                assert.equal(results[2].player.id, results[1].opponent.player.id, "Third player should be the second's opponent.");
                assert.equal(results[3].player.id, results[0].opponent.player.id, "Fourth player should be the first's opponent.");
                done();
            }).catch((err) => {
                done(err);
            });
        });

        it('should not find any matches for players due to timeout', (done) => {
            const matchmakerInstance = matchmaker.initMatchmaker(playerPool.initPlayerPool(constants.GAMES.DOTA2), {
                game: constants.GAMES.DOTA2,
                iterationDelay: 200,
                iterationDistance: 5,
                maxTime: 1000
            });

            let matchForPlayer1;
            let matchForPlayer2;

            matchForPlayer1 = matchmakerInstance.findMatchForPlayer(playerMock.buildDotaPlayer(1, 1100));

            matchForPlayer2 = Promise.delay(500).then(() => {
                return matchmakerInstance.findMatchForPlayer(playerMock.buildDotaPlayer(2, 4400));
            });

            Promise.all([matchForPlayer1, matchForPlayer2,]).then((results) => {
                assert.equal(results[0].opponent, null);
                assert.equal(results[1].opponent, null);
                done();
            }).catch((err) => {
                done(err);
            });
        });
    });
});