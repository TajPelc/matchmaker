"use strict";
const _ = require('lodash');

const pewdiepie =
    {
        id: "1",
        verified: true,
        confirmationcode: "",
        sessiontoken: "b3e36722def06efb9f4e1dc4de1647dee24ec93b9dfc7198e60aae14ded3bbcc",
        username: "pewdiepie",
        email: "felix@pewdipie.com",
        salt: "salt'n'peppa",
        saltedpassword: "539e95fb767dc915f882f24c5ba37eb10b8c551d6d7af8bf0651fee3ab1ab86a",
        referralcode: null,
        address: null,
        city: null,
        state: null,
        zip: null,
        birthdate: null,
        lolaccount: null,
        dota2account: null,
        ethereumaccount: null,
        lolmmr: null,
        dota2mmr: "4015"
    };

function buildDotaPlayer(id, mmr) {
    mmr = mmr || "4015";
    const player = _.clone(pewdiepie);
    player.id = id.toString();
    player.email = `player-${id}@dota2.com`;
    player.dota2mmr = mmr;
    return player;
}

module.exports = {
    buildDotaPlayer
};