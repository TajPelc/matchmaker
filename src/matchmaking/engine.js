"use strict";

const Promise = require('bluebird');
const _ = require('lodash');
const constants = require('../config/constants');
const errors = require('../config/errors');
const promiseTools = require('../toolbox/promise');
const debug = require('debug')('matchmaker:engine');

/**
 *
 * @param playerPool
 * @param config
 * @returns {{findMatchForPlayer: (function(*=))}}
 */
function engine(playerPool, config) {
    if (!_.isObject(config)) throw new errors.InvalidConfig("Invalid game engine confing provided.");
    if (!_.includes(constants.SUPPORTED_GAMES, config.game)) throw new errors.InvalidGameError(`Initializing matchmaker for an invalid game ${config.game}`);

    const game = config.game;
    const iterationDelay = config.iterationDelay;
    const iterationDistance = config.iterationDistance;
    const maxTime = config.maxTime;
    const maxIterations = parseInt(maxTime / iterationDelay);

    /**
     * Each player that joins starts his own search until a match is found
     * @type {{}}
     */
    const activeSearches = {};

    function matchOneVsOne(player) {
        const matchResult = {iteration: 1, iterationDistance: iterationDistance, player: player, opponent: null};

        let stopSearching = false;
        let externalOpponent = null;
        function stop(opponent) {
            stopSearching = true;
            externalOpponent = opponent;
        }

        /**
         * Find a match for the player
         * @returns {Promise.<TResult>}
         */
        function search() {
            debug(`P${player.id} ⇒ `);

            playerPool.addPlayer(player);

            /**
             * Search until a match is found
             */
            return promiseTools.promiseFor((matchResult) => {
                /**
                 * Determine if searching should continue
                 */
                if (stopSearching) return false; // force stop
                if (_.isObject(matchResult.opponent)) return false; // player found
                return matchResult.iteration <= maxIterations;
            }, (matchResult) => {
                /**
                 * Try to find a match in the player pool
                 */
                debug(`P${player.id} ↻ ${matchResult.iteration} ↔ ${matchResult.iterationDistance || '∞'}`);
                return Promise.delay(iterationDelay)
                    .then(() => {
                        matchResult.opponent = playerPool.findNearestPlayerFor(player, matchResult.iterationDistance);
                        playerPool.removePlayer(player);
                        playerPool.removePlayer(matchResult.opponent.player);

                        return matchResult;
                    })
                    .catch(errors.EmptyPoolError, () => {
                        matchResult.iteration++;
                        return matchResult;
                    })
                    .catch(errors.NoSuitablePlayersFound, () => {
                        matchResult.iteration++;
                        matchResult.iterationDistance += iterationDistance;
                        return matchResult;
                    });
            }, matchResult).then((matchResult) => {
                /**
                 * Match was found from the other side
                 */
                if (_.isObject(externalOpponent)) {
                    matchResult.opponent = externalOpponent;
                    debug(`P${player.id} ⇐ P${matchResult.opponent.player.id}`);

                    return matchResult;
                }

                /**
                 * No match was found
                 */
                if (!_.isObject((matchResult.opponent))) {
                    debug(`P${player.id} ✘`);
                    return matchResult;
                }

                debug(`P${player.id} ⇒ P${matchResult.opponent.player.id}`);

                return matchResult;
            });
        }

        return {
            search,
            stop
        }
    }

    return {
        findMatchForPlayer: (player) => {
            activeSearches[player.id] = matchOneVsOne(player);
            return activeSearches[player.id].search().then((matchResult) => {
                if (matchResult.opponent) {
                    const opponentId = matchResult.opponent.player.id;

                    activeSearches[opponentId].stop({player: player, distance: matchResult.opponent.distance});
                }

                return matchResult;
            });
        }
    };
}

module.exports = {
    initMatchmaker: engine
};