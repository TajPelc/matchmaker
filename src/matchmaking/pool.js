"use strict";
const _ = require('lodash');
const constants = require('../config/constants');
const errors = require('../config/errors');

function playerPool(game) {
    if (!_.includes(constants.SUPPORTED_GAMES, game)) throw new errors.InvalidGameError(`Initializing matchmaker for an invalid game ${game}`);

    let playerPool = [];
    let matched = [];
    let mmrProperty = `${game}mmr`;

    function addPlayer(player) {
        if (!_.isPlainObject(player)) throw new errors.InvalidPlayerError('Invalid player passed');

        playerPool.push(player);
    }

    function removePlayer(player) {
        playerPool = _.reject(playerPool, ['id', player.id]);
    }

    function getPoolSize() {
        return playerPool.length;
    }

    function findNearestPlayerFor(player, maxDistance) {
        let searchablePool = _.reject(playerPool, ['id', player.id]);

        if (searchablePool.length < 1) {
            throw new errors.EmptyPoolError("No players in the pool");
        }

        let nearestPlayer = searchablePool[0];
        let distance = Math.abs(player[mmrProperty] - nearestPlayer[mmrProperty]);

        for (let i = 0; i < searchablePool.length; i++) {
            let newDistance = Math.abs(player[mmrProperty] - searchablePool[i][mmrProperty]);
            if (newDistance < distance) {
                distance = newDistance;
                nearestPlayer = searchablePool[i];
            }
        }

        if (maxDistance && distance > maxDistance) {
            throw new errors.NoSuitablePlayersFound("No suitable players could be found");
        }

        matched.push(player.id);
        matched.push(nearestPlayer.id);

        return {player: nearestPlayer, distance};
    }

    return {
        addPlayer,
        removePlayer,
        getPoolSize,
        findNearestPlayerFor
    };
}

module.exports = {
    initPlayerPool: playerPool
};