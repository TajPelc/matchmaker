"use strict";

const connectionString = process.env.DATABASE_URL || 'postgres://localhost:5432/matchmaking';

const Promise = require('bluebird');
const pg = require('pg-promise')({
    promiseLib: Promise
});

module.exports = pg(connectionString);