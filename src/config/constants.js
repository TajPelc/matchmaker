"use strict";

const GAMES = {
    DOTA2: 'dota2',
    LOL: 'lol'
};

module.exports = {
    GAMES,
    SUPPORTED_GAMES: [GAMES.DOTA2, GAMES.LOL]
};