"use strict";
class MatchmakerError extends Error {
    constructor(message) {
        super(message);
        this.name = this.constructor.name;
        if (typeof Error.captureStackTrace === 'function') {
            Error.captureStackTrace(this, this.constructor);
        } else {
            this.stack = (new Error(message)).stack;
        }
    }
}

class InvalidGameError extends MatchmakerError {}
class InvalidPlayerError extends MatchmakerError {}
class NoSuitablePlayersFound extends MatchmakerError {}
class EmptyPoolError extends MatchmakerError {}
class InvalidConfig extends MatchmakerError {}

module.exports = {
    InvalidGameError,
    InvalidPlayerError,
    NoSuitablePlayersFound,
    EmptyPoolError,
    InvalidConfig
};