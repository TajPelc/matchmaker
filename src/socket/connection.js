"use strict";

const matchmaker = require('../matchmaking/engine');
const playerPool = require('../matchmaking/pool');
const constants = require('../config/constants');
const errors = require('../config/errors');
const playerMock = require('../../test/mocks/player');
const _ = require('lodash');

const matchmakerInstance = matchmaker.initMatchmaker(playerPool.initPlayerPool(constants.GAMES.DOTA2), {
    game: constants.GAMES.DOTA2,
    iterationDelay: 1000, // 0.1s between iterations
    iterationDistance: null,
    maxTime: 5 * 60 * 1000 // 5 minutes
});

const sockjs = require('sockjs');
const debug = require('debug')('matchmaker:socket');

const channel = sockjs
    .createServer({sockjs_url: 'http://cdn.jsdelivr.net/sockjs/1.0.1/sockjs.min.js'})
    .on('connection', function (connection) {
        debug(`${connection.id}:status: OPEN`);

        connection.on('data', function (payload) {
            try {
                const message = JSON.parse(payload);

                debug(`${connection.id}:downstream:`, message);

                if (message.type === 'initMatchmaking') {
                    const player = playerMock.buildDotaPlayer(parseInt(Math.random() * 100000), parseInt(Math.random() * 4000));
                    player.username = message.player;

                    matchmakerInstance.findMatchForPlayer(player).then((match) => {
                        const response = {type: 'matchFound', match: {
                            player: {
                                username: match.player.username,
                                mmr: match.player[`${constants.GAMES.DOTA2}mmr`]
                            },
                            opponent: {
                                username: match.opponent.player.username,
                                mmr: match.opponent.player[`${constants.GAMES.DOTA2}mmr`],
                                distance: match.opponent.distance
                            }
                        }};

                        debug(`${connection.id}:upstream:`, response);

                        connection.write(JSON.stringify(response));
                    });
                }
            } catch (e) { // ignore non-json messages
                debug('Unable to parse JSON.');
            }
        });

        connection.on('close', function () {
            debug(`${connection.id}:status: CLOSED`);
        });
    });

module.exports = channel;