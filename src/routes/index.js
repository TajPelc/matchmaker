"use strict";

const express = require('express');
const router = express.Router();
const db = require('../db/connect');

router.get('/', function (req, res, next) {
    db.any("select * from users", [true])
        .then((users) => {
            res.json({"users": users});
        });
});

module.exports = router;
