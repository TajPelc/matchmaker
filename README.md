# Matchmaker Engine
Installation instructions

  - `yarn install` to install dependencies
  - `yarn test` for running unit tests
  - `yarn start` to build the front-end and start the service

# How to use

Open a tab on `localhost:3000`, enter a player name and press search. Open another tab. Repeat. The players find each other and are given a match.

# Behind the scenes

The frontend establishes a socket connection and sends the player name. The player is added to the pool of players and matched when anothe player joins. MMR is randomly assigned.

When creting the matchmaker if supplied with a matchmaking rating distance the engine prefers to pair players together that are closer in skill. As the search goes on, the matchmaking becomes less and less picky about the player that is a good match.

This is demonstrated in the tests.

# Further steps
This is just a basic proof-of-concept.

Further steps:
  - 2v2, 3v3, 5v5 matches.
  - Take into account the *value* of the game by prefering higher stake games.
  - Searching with a party.
  - Improving the stability. Lots of checks missing.

