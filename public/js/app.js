"use strict";

const SockJS = require('sockjs-client');
const search = document.getElementById('search');
const searching = document.getElementById('searching');
const player = document.getElementById('player');
const status = document.getElementById('status');

function addMessage(message) {
    const time = new Date();
    const timestamp = '['
        + (time.getMinutes() < 10 ? '0' : '') + time.getHours()
        + ':' + (time.getMinutes() < 10 ? '0' : '') + time.getMinutes()
        + ':' + (time.getSeconds() < 10 ? '0' : '') + time.getSeconds()
        + '.' + time.getMilliseconds()
        + ']';
    const div = document.createElement('div');
    div.innerText = timestamp + ' ' + message;
    status.appendChild(div);
    div.scrollIntoView();
}

function enterSearchState() {
    search.setAttribute('disabled', 'disabled');
    player.setAttribute('disabled', 'disabled');
    searching.className = 'loader';
}

function leaveSearchState() {
    search.removeAttribute('disabled');
    player.removeAttribute('disabled');
    searching.className = 'hidden';
}

const socketOpening = new Promise(function(resolve) {
    const socket = new SockJS('/channel');

    socket.onopen = function () {
        console.log('[*] open', socket.protocol);
        addMessage('Connected to matchmaking.');
        resolve(socket);
    };

    socket.onmessage = function (payload) {
        console.log('[.] message', payload.data);

        try {
            const message = JSON.parse(payload.data);

            switch (message.type) {
                case 'matchFound':
                    addMessage('You vs ' + message.match.opponent.username);
                    addMessage(message.match.opponent.distance + ' mmr difference! Get rekt.');

                    leaveSearchState();
                    break;
                default:
                    leaveSearchState();
            }
        } catch (e) {
        }
    };
    socket.onclose = function () {
        console.log('[*] close');
        addMessage('Disconnected from matchmaking.');
    };
});

search.addEventListener('click', function () {
    socketOpening.then(function(socket) {
        enterSearchState();
        socket.send(JSON.stringify({type: "initMatchmaking", player: player.value}));
    });
});