const config = {
    context: __dirname + '/public',
    devtool: 'inline-source-map',
    entry: {
        app: './js/app.js',
    },
    output: {
        path: __dirname + '/public/dist',
        publicPath: '/dist/',
        filename: 'bundle.js',
    },
    devServer: {
        port: 8080,
        contentBase: __dirname + '/public',
        proxy: {
            '/channel': {
                target: 'http://localhost:3000',
                secure: false
            }
        }
    }
};

module.exports = config;